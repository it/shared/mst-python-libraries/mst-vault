# mst-vault

[[_TOC_]]

## Installation

```bash
pip install mst-vault --index-url https://git.mst.edu/api/v4/groups/it%2Fshared%2Fmst-python-libraries/-/packages/pypi/simple
```

requirements.txt

```text
--extra-index-url https://git.mst.edu/api/v4/groups/it%2Fshared%2Fmst-python-libraries/-/packages/pypi/simple
mst-vault
```

## Usage

```python
from mst.vault import MSTVault

vault = MSTVault()

secrets = vault.read_secret("path/to/secret")
```
